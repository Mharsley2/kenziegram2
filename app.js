const express = require('express');
const fs = require('fs');
const multer = require('multer');
const bodyParser = require('body-parser');
const path = require('path')

const app = express();

app.set('views',path.join(__dirname,'views'));
app.set('view engine', 'pug');


const storage = multer.diskStorage({
    destination: "public/uploads/",
    filename: function (req, file, cb) {
        cb(
            null,
            file.fieldname + "-" + Date.now() + path.extname(file.originalname)
        );
    }
});

const upload = multer({
    storage: storage
}).single('fileupload');

app.use(express.static('./public'));
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/', (req, res) => {

    let paths = './public/uploads'; 
    fs.readdir(paths, function(err, items) {   
        res.render('index', { pictureNames: items })
    });
    
});

 app.post('/uploads', upload, (req, res) => {
    upload(req, res, (err) => {
        // if (err) {
        //     res.render('index' , {
        //         msg: err
        //     }) 
        // }else {
        //     if(req.file == undefined) {
        //         res.render('index' , {
        //             msg: 'Error no file selected'
        //         }) 
//             }else {
    //                 })
    //             }
    //         }
                            res.render('second', {               
                            msg: 'File Uploaded!',
                            file:`uploads/${req.file.filename}`})
                        })})

//     })

//  })


const port = 3000;
app.listen(port, () => console.log(`Server started on ${port}`));

